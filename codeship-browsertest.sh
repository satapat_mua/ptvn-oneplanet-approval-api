#!/bin/bash

# Set value to loops (normal = 12.5 m, curl timeout = 25 m)
DEPLOY_STATUS=fail
iversion=$CI_BUILD_NUMBER
loops=150
timesleep=5
timeout=5
x=1

# Check branch deployment
substring='feature'
if [ "${CI_BRANCH/$substring}" = "$CI_BRANCH" ] ; then
	base_url='http://52.221.42.75/version.txt'
else
    base_url='http://52.221.42.75/version.txt'
fi

echo "Branch : $CI_BRANCH"
echo "Expected Version : $iversion"

# Loop curl check version
while [ $x -le $loops ]
do
	content=$(curl -L -s $base_url --connect-timeout $timeout | grep -c "$iversion")
	# Check fond new version
	if [ $content == 1 ]
	then
	    DEPLOY_STATUS=pass
		x=$(( $loops + 1))
	  	if [ $x == $(( $loops + 1)) ]
	    then
	       printf ".\nVersion status : new\n"
	    else
	  	    printf "\nVersion status : new\n"
	  	fi
	# Check found old version or timeout or fail
	else
	    x=$(( $x + 1 ))
	    if [ $x == $(( $loops + 1)) ]
	    then
	       printf "\nVersion status : old\n"
	    else
	  	    echo -n "."
	  	    sleep $timesleep
	  	fi
	fi
done

export DEPLOY_STATUS

echo "Deployment status : $DEPLOY_STATUS"
