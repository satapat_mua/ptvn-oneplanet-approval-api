# Spring Boot image with build artifact

FROM ascendcorp/ptvn-docker-base:openjdk-8u121-jdk

ENV APP_ROOT=/opt/app \
    JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre \
    LOG_LOCATION=/var/opt/ptvn-admin \
    TEMP_LOCATION=/opt/app/temp

WORKDIR $APP_ROOT

EXPOSE 80 8080

### CONFIGURATION

# START
ENTRYPOINT ["start.sh"]

COPY . /tmp/app

# Config filebeat
# Generate a version of Application
# Build Application
# Switch From JDK To JRE & Remove Unused packages
# Optimize images

RUN mkdir -p ${APP_ROOT} \
    && mkdir -p ${LOG_LOCATION} \
    && mkdir -p ${TEMP_LOCATION} \
    && cd /tmp/app \

    && cp opsfiles/filebeat.yml /etc/filebeat/filebeat.yml \
    && cp opsfiles/start.sh /usr/local/bin/start.sh \
    && chmod +x /usr/local/bin/start.sh \

    && export SOURCE_COMMIT="${SOURCE_COMMIT:-$(git rev-parse HEAD)}" \
    && export SOURCE_TAG="${SOURCE_TAG:-$(git describe --tags --abbrev=0)}" \
    && echo -n > version.txt \
    && echo 'SOURCE_COMMIT='${SOURCE_COMMIT} >> version.txt \
    && echo 'SOURCE_TAG='${SOURCE_TAG} >> version.txt \
    && cp version.txt /tmp/app/src/main/resources/static \

    && gradle build -x test -x check \
    && mv build/libs/ptvn-oneplanet-approval-api.jar ${APP_ROOT}/app.jar \
    && rm -rf /tmp/app \
    && rm -rf /home/* \

    && apt-get purge -y --auto-remove openjdk-8-jdk curl wget unzip bzr git mercurial openssh-client subversion python \
    && set -x \
	&& [ "$JAVA_HOME" = "$(docker-java-home)" ] \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
