package com.oneplanet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.oneplanet.core.security.AuditorAwareBean;

/**
 * Created by sattra.sow on 8/18/2017 AD.
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(value = "com.oneplanet")
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaConfig {

    @Bean
    public AuditorAware<String> auditorProvider() {
        return new AuditorAwareBean();
    }

}
