package com.oneplanet.config;

/**
 * Created by sutee.cha on 30/1/2018 AD.
 */
public class DatabaseSetting {
    public static String host;
    public static String driver;
    public static String username;
    public static String password;

    public static void setDatabaseConfig(String host, String driver, String username, String password) {
        DatabaseSetting.host = host;
        DatabaseSetting.driver = driver;
        DatabaseSetting.username = username;
        DatabaseSetting.password = password;
    }
}