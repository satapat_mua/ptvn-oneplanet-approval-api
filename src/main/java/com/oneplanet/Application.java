package com.oneplanet;

import java.util.TimeZone;
import javax.annotation.PostConstruct;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot main application class.
 *
 * @author Matt Warman
 */
@SpringBootApplication
@EnableAutoConfiguration
public class Application {

    /**
     * The name of the Cache for Greeting entities.
     */
    public static final String CACHE_GREETINGS = "greetings";

    /**
     * Entry point for the application.
     *
     * @param args Command line arguments.
     */
    public static void main(final String... args) {
        SpringApplication app = new SpringApplication(Application.class);

        //do not show Spring boot banner when boot starts!!!!!
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

}
