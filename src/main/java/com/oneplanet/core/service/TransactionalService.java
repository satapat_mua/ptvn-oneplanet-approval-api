package com.oneplanet.core.service;

import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;

import com.oneplanet.core.model.DocumentNumber;
import com.oneplanet.core.model.DocumentType;
import com.oneplanet.core.model.UserSearch;
import com.oneplanet.core.repository.jpa.DocumentNumberRepository;
import com.oneplanet.core.repository.solr.UserSearchRepository;

/**
 * Created by sattra.sow on 7/26/2017 AD.
 */
@Log
public abstract class TransactionalService {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private DocumentNumberRepository documentNumberRepository;

    @Autowired
    private UserSearchRepository userSearchRepository;

    @Transactional(propagation = Propagation.MANDATORY)
    public String generateNumber(Long buyerId, DocumentType prefix) {
        final String year = Year.now().format(DateTimeFormatter.ofPattern("yy"));
        final String month = YearMonth.now().format(DateTimeFormatter.ofPattern("MM"));
        DocumentNumber documentNumber = null;
        if (prefix.equals(DocumentType.RE)) {
            documentNumber = documentNumberRepository.findByBuyerIdAndPrefixAndYearAndMonth(buyerId, DocumentType.GR, year, month);
        } else {
            documentNumber = documentNumberRepository.findByBuyerIdAndPrefixAndYearAndMonth(buyerId, prefix, year, month);
        }

        if (documentNumber == null) {
            documentNumber = new DocumentNumber();
            documentNumber.setBuyerId(buyerId);
            documentNumber.setPrefix(prefix);
            documentNumber.setYear(year);
            documentNumber.setMonth(month);
            documentNumber.setRunningNo(0L);
        }

        long runningNo = documentNumber.getRunningNo();
        documentNumber.setRunningNo(++runningNo);
        documentNumberRepository.save(documentNumber);

        if (runningNo > 99999) {
            return String.format("%s%03d-%s%s%d", prefix, buyerId, year, month, runningNo);
        }
        return String.format("%s%03d-%s%s%05d", prefix, buyerId, year, month, runningNo);
    }

    protected UserSearch findUser(Long userId) {
        return userSearchRepository.findByUserId(userId);
    }

    protected void log(Object obj) {
        try {
            LOG.info(MAPPER.writeValueAsString(obj));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void setDocumentNumberRepository(DocumentNumberRepository documentNumberRepository) {
        this.documentNumberRepository = documentNumberRepository;
    }

    public void setUserSearchRepository(UserSearchRepository userSearchRepository) {
        this.userSearchRepository = userSearchRepository;
    }
}
