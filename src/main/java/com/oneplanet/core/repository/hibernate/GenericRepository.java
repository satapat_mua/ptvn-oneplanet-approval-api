package com.oneplanet.core.repository.hibernate;

import static com.oneplanet.core.repository.hibernate.ConcatenableIlikeCriterion.ilike;

import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public abstract class GenericRepository<T> {
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Transactional
    public void create(final T clazz) throws HibernateException {
        getCurrentSession().persist(clazz);
    }

    @Transactional
    public void batchCreate(Set<T> itemList) throws HibernateException {
        int itemCount = 0;
        for (T t : itemList) {
            create(t);
            if (itemCount % 30 == 0) {
                getCurrentSession().flush();
                getCurrentSession().clear();
            }
            ++itemCount;
        }
    }

    @Transactional
    public void delete(final T clazz) throws HibernateException {
        getCurrentSession().delete(clazz);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public T update(final T clazz) throws HibernateException {
        return (T) getCurrentSession().merge(clazz);
    }

    @Transactional
    public void batchUpdate(Set<T> itemList) throws HibernateException {

        int itemCount = 0;
        for (T t : itemList) {
            update(t);
            if (itemCount % 30 == 0) {
                getCurrentSession().flush();
                getCurrentSession().clear();
            }
            ++itemCount;
        }
    }

    protected void addRestrictionIfNotNull(Criteria criteria, String propertyName, Object value) {
        if (value != null) {
            criteria.add(Restrictions.eq(propertyName, value));
        }
    }

    protected void addRestrictionLikeIfNotNull(Criteria criteria, String propertyName, String value) {
        if (value != null && value != "") {
            criteria.add(Restrictions.like(propertyName, value, MatchMode.ANYWHERE));
        }
    }

    protected void addRestrictionLikeIfNotNullWithOrCondition(Criteria criteria, String propertyName1, String propertyName2, String value) {
        if (value != null && value != "") {
            Criterion res1 = Restrictions.like(propertyName1, value, MatchMode.ANYWHERE);
            Criterion res2 = Restrictions.like(propertyName2, value, MatchMode.ANYWHERE);
            criteria.add(Restrictions.or(res1, res2));
        }
    }

    protected void addRestrictionInIfNotEmpty(Criteria criteria, String propertyName, List value) {
        if (value != null && value.size() > 0) {
            criteria.add(Restrictions.in(propertyName, value));
        }
    }

    protected void concatenableLike(Criteria criteria, String value, String... columns) {
        if (value != null && value != "") {
            criteria.add(ilike(value, MatchMode.ANYWHERE, " ", columns));
        }
    }

    @Bean
    public SessionFactory getSessionFactory() {
        if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        return entityManagerFactory.unwrap(SessionFactory.class);
    }

    protected final Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }
}
