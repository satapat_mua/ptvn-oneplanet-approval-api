package com.oneplanet.core.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oneplanet.core.model.DocumentNumber;
import com.oneplanet.core.model.DocumentType;

/**
 * Created by sattra.sow on 7/26/2017 AD.
 */
public interface DocumentNumberRepository extends JpaRepository<DocumentNumber, Long> {

    DocumentNumber findByBuyerIdAndPrefixAndYearAndMonth(Long buyerId, DocumentType prefix, String year, String month);

}
