package com.oneplanet.core.repository.solr;

import org.springframework.data.solr.repository.SolrCrudRepository;

import com.oneplanet.core.model.UserSearch;

/**
 * Created by sattra.sow on 8/7/2017 AD.
 */
public interface UserSearchRepository extends SolrCrudRepository<UserSearch, Long> {

    UserSearch findByUserId(Long userId);

}
