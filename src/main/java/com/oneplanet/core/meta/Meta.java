package com.oneplanet.core.meta;

import lombok.Data;

import com.oneplanet.core.Pagination;

@Data
public class Meta {

    private Pagination pagination;

    public Meta() {

    }

    public Meta(Pagination pagination) {
        this.pagination = pagination;
    }
}
