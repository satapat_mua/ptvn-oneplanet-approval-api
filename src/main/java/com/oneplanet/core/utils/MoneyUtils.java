package com.oneplanet.core.utils;

import static org.hibernate.type.DoubleType.ZERO;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by sattra.sow on 8/21/2017 AD.
 */
public class MoneyUtils {

    private static final MathContext MATH_CONTEXT = new MathContext(32, RoundingMode.HALF_UP);

    public static BigDecimal amount(Double amount) {
        if (amount == null) {
            return BigDecimal.ZERO;
        }

        BigDecimal val = new BigDecimal(amount, MATH_CONTEXT);
        val = val.setScale(4, BigDecimal.ROUND_HALF_UP);
        return val;
    }

    public static Double value(BigDecimal amount) {
        if (amount == null) {
            return ZERO;
        }

        BigDecimal val = new BigDecimal(amount.doubleValue(), MATH_CONTEXT);
        val = val.setScale(4, BigDecimal.ROUND_HALF_UP);
        return val.doubleValue();
    }

    public static BigDecimal value(Double amount) {
        if (amount == null) {
            return BigDecimal.ZERO;
        }

        BigDecimal val = new BigDecimal(amount, MATH_CONTEXT);
        val = val.setScale(4, BigDecimal.ROUND_HALF_UP);
        return val;
    }
}
