package com.oneplanet.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by sattra.sow on 8/8/2017 AD.
 */
public class DateUtils {

    public static final String UTC_TIMEZONE_STRING = "UTC";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private static final SimpleDateFormat DATE_FORMAT_MILLI_SEC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private static final String DATE_FORMAT_WITHOUT_TIME = "dd/MM/yyyy";

    public static Date asDate(String date) {
        if (date == null) {
            return null;
        }
        try {
            return DATE_FORMAT.parse(date.replaceAll("Z$", "+0000"));
        } catch (ParseException exception) {
            throw new RuntimeException(exception);
        }
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date dateWithTime(Date date) {
        if (date == null) {
            return null;
        }
        String shippingDate = date.toInstant().toString();
        String shippingDateOnly = shippingDate.substring(0, shippingDate.indexOf("T"));
        String currentDateTime = new Date().toInstant().toString();
        String currentTime = currentDateTime.substring(
                    currentDateTime.indexOf("T") + 1,
                    currentDateTime.indexOf("Z"));
        try {
            return DATE_FORMAT_MILLI_SEC.parse(shippingDateOnly + 'T' + currentTime);
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDate asLocalDate(Date date, ZoneId zoneId) {
        return Instant.ofEpochMilli(date.getTime()).atZone(zoneId).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDateTime asLocalDateTime(Date date, ZoneId zoneId) {
        return Instant.ofEpochMilli(date.getTime()).atZone(zoneId).toLocalDateTime();
    }

    public static Date startOfDay(Date date) {
        return asDate(asLocalDate(date).atStartOfDay());
    }

    public static Date endOfDay(Date date) {
        return asDate(asLocalDate(date).atStartOfDay().plusDays(1L).minusSeconds(1));
    }

    public static String format(Date date) {
        return DATE_FORMAT.format(date).replaceAll("\\+0000", "Z" );
    }

    public static Date getDateWithTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Date getDateWithTimeZone(Date date, String timezone) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getTimeZone(timezone));
        return cal.getTime();
    }

    public static Date getFirstDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getFirstDateOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getFirstDateOfLastMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getLastDateOfLastMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getFirstDateOfLastYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, -1);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getLastDateOfLastYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, -1);
        cal.set(Calendar.MONTH, 11);
        cal.set(Calendar.DAY_OF_MONTH, 31);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Date addDay(Date date, Integer noOfDay, String timezone) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.setTimeZone(TimeZone.getTimeZone(timezone));
        cal.add(Calendar.DATE, noOfDay);
        return cal.getTime();
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date addDaysTime(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date addHours(Date date, int hours) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, hours);
        return cal.getTime();
    }

    public static Long getHourOffsetFromTimeZone(String timezone) {
        Long hour = TimeUnit.MILLISECONDS.toHours(Long.valueOf(TimeZone.getTimeZone(timezone).getRawOffset()));
        return hour;
    }

    public static int zonedDateTimeDifferenceHours(String d1, String d2) {
        Date currentDate = new Date();
        int d1Int = currentDate.toInstant().atZone(ZoneId.of(d1)).getOffset().getTotalSeconds();
        int d2Int = currentDate.toInstant().atZone(ZoneId.of(d2)).getOffset().getTotalSeconds();
        int result = (d1Int - d2Int) / 3600;
        return result;
    }

    public static Date convertDateBetweenTimezone(Date date, String d1, String d2) {
        Date currentDate = new Date();
        int d1Int = currentDate.toInstant().atZone(ZoneId.of(d1)).getOffset().getTotalSeconds();
        int d2Int = currentDate.toInstant().atZone(ZoneId.of(d2)).getOffset().getTotalSeconds();
        int hours = (d2Int - d1Int) / 3600;

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, hours);

        return cal.getTime();
    }

    public static String displayTimeZone(TimeZone tz) {
        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        minutes = Math.abs(minutes);
        return String.format((hours >= 0) ? "(GMT+%02d:%02d)" : "(GMT%03d:%02d)", hours, minutes);
    }

    public static String formatDateWithGmt(Date date, String timeZone) {
        String result = "";
        if (date != null) {
            SimpleDateFormat sf = new SimpleDateFormat(DATE_FORMAT_WITHOUT_TIME);
            result = String.format("%s %s", sf.format(date), displayTimeZone(TimeZone.getTimeZone(timeZone)));
        }
        return result;
    }

    public static String convertDateTimeZoneWithGmt(Date date, String timeZone, Boolean isTime) {
        String result = "";
        if (date != null) {
            SimpleDateFormat sf = new SimpleDateFormat(DATE_FORMAT_WITHOUT_TIME);
            sf.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date currentDateTimeZone = getDateWithTimeZone(date, timeZone);
            result = sf.format(currentDateTimeZone);
            if (Boolean.TRUE.equals(isTime)) {
                result = String.format("%s %s", result, displayTimeZone(TimeZone.getTimeZone(timeZone)));
            }
        }
        return result;
    }
}
