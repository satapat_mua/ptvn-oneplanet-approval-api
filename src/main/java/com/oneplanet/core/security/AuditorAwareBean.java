package com.oneplanet.core.security;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import lombok.extern.java.Log;

/**
 * Created by sattra.sow on 8/18/2017 AD.
 */
@Log
public class AuditorAwareBean implements AuditorAware<String> {

    @Autowired
    ServletContext servletContext;

    @Override
    public String getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // TODO: Implement authentication
        if (authentication == null || !authentication.isAuthenticated()) {
            return "SYSTEM";
        }

        // TODO: anonymousUser is temporary allowed, it requires to be consistent with Security implementation ASAP.
        if (servletContext.getAttribute("firstName") != null && servletContext.getAttribute("lastName") != null) {
            String userId = (String) servletContext.getAttribute("userId");
            return userId;
        } else {
            return (String) authentication.getPrincipal();
        }
    }

}
