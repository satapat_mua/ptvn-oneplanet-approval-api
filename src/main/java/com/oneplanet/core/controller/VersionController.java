package com.oneplanet.core.controller;

import java.io.IOException;
import java.net.URL;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;

@RestController
public class VersionController {

    @RequestMapping(value = "/version",
            method = RequestMethod.GET)
    public String getVersion() throws IOException {
        URL url = Resources.getResource("static/version.txt");

        return Resources.toString(url, Charsets.UTF_8);
    }
}
