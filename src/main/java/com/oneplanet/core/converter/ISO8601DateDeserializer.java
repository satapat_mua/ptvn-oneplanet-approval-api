package com.oneplanet.core.converter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import com.oneplanet.core.utils.DateUtils;

/**
 * Created by sattra.sow on 10/31/2017 AD.
 */
public class ISO8601DateDeserializer extends StdDeserializer<Date> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public ISO8601DateDeserializer() {
        this(null);
    }

    public ISO8601DateDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Date deserialize(JsonParser jsonparser, DeserializationContext context) throws IOException, JsonProcessingException {
        try {
            Date date = DATE_FORMAT.parse(jsonparser.getText().replaceAll("Z$", "+0000"));

            // Convert GMT+7 to UTC
            LocalDateTime localDateTime = DateUtils.asLocalDateTime(date, ZoneId.of("Asia/Bangkok"));
            localDateTime.atZone(ZoneId.systemDefault());

            return DateUtils.asDate(localDateTime);
        } catch (ParseException exception) {
            throw new RuntimeException(exception);
        }
    }

}
