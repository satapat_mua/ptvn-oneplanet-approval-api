package com.oneplanet.core.converter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import com.oneplanet.core.utils.DateUtils;

/**
 * Created by sattra.sow on 10/31/2017 AD.
 */
public class ISO8601DateSerializer extends StdSerializer<Date> {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    public ISO8601DateSerializer() {
        this(null);
    }

    public ISO8601DateSerializer(Class clazz) {
        super(clazz);
    }

    @Override
    public void serialize(Date value, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
        LocalDateTime localDateTime = DateUtils.asLocalDateTime(value);
        // Convert UTC to GMT+7
        generator.writeString(DATE_FORMAT.format(localDateTime.atZone(ZoneId.of("Asia/Bangkok"))));
    }

}
