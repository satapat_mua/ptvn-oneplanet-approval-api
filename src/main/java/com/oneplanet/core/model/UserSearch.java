package com.oneplanet.core.model;

import javax.persistence.Id;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;
import lombok.Data;

/**
 * Created by sattra.sow on 8/7/2017 AD.
 */
@Data
@SolrDocument(solrCoreName = "organization_users")
public class UserSearch {

    @Id
    @Indexed
    @Field("id")
    private String id;

    @Field("userId_i")
    private Integer userId;

    @Field("firstName_s")
    private String firstName;

    @Field("lastName_s")
    private String lastName;

    @Field("email_s")
    private String email;

    @Field("phone_s")
    private String phone;

}
