package com.oneplanet.core.model;

import javax.persistence.Embeddable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by sattra.sow on 7/27/2017 AD.
 */
@Data
@EqualsAndHashCode(exclude = "addressId")
@Embeddable
public class Address implements Comparable<Address> {

    private Long addressId;

    private String name;

    private String address;

    private String province;

    private String zipCode;

    private String country;

    @Override
    public int compareTo(Address other) {
        if (addressId == null && other.getAddressId() == null) {
            return 0;
        } else if (addressId == null && other.getAddressId() != null) {
            return -1;
        } else if (addressId != null && other.getAddressId() == null) {
            return 1;
        }
        return addressId.compareTo(other.getAddressId());
    }

}
