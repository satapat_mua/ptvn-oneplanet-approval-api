package com.oneplanet.core.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import lombok.Data;

/**
 * The parent class for all transactional persistent entities.
 *
 * @author Matt Warman
 */
@Data
@MappedSuperclass
public class TransactionalEntity implements Serializable {

    @Version
    @Column(name = "Version")
    private Long version;

    @Column(name = "CreatedAt")
    @CreatedDate
    private Date createdAt;

    @Column(name = "CreatedBy")
    @CreatedBy
    private String createdBy;

    @Column(name = "UpdatedAt")
    @LastModifiedDate
    private Date updatedAt;

    @Column(name = "UpdatedBy")
    @LastModifiedBy
    private String updatedBy;

}
