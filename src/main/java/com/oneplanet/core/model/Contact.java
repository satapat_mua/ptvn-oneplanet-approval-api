package com.oneplanet.core.model;

import javax.persistence.Embeddable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by sattra.sow on 7/27/2017 AD.
 */
@Data
@EqualsAndHashCode(exclude = "contactId")
@Embeddable
public class Contact implements Comparable<Contact> {

    private Long contactId;

    private String name;

    private String phone;

    private String email;

    private String mailStop;

    @Override
    public int compareTo(Contact other) {
        if (contactId == null && other.getContactId() == null) {
            return 0;
        } else if (contactId == null && other.getContactId() != null) {
            return -1;
        } else if (contactId != null && other.getContactId() == null) {
            return 1;
        }
        return contactId.compareTo(other.getContactId());
    }
}
