package com.oneplanet.core.model;

/**
 * Created by sattra.sow on 7/26/2017 AD.
 */
public enum DocumentType {
    PR, PO, GR, RE
}
