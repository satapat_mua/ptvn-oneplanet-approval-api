package com.oneplanet.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by sattra.sow on 7/26/2017 AD.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "DocumentNumber")
public class DocumentNumber {

    @Id
    @GenericGenerator(
            name = "document_number_generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "seq_document_number"),
                    @Parameter(name = "initial_value", value = "1000"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @GeneratedValue(generator = "document_number_generator")
    @Column(name = "Id")
    private Long id;

    @Version
    @Column(name = "Version")
    private Long version;

    @NotNull
    @Column(name = "BuyerId")
    private Long buyerId;

    @NotNull
    @Column(name = "Prefix")
    @Enumerated(EnumType.STRING)
    private DocumentType prefix;

    @NotNull
    @Column(name = "Year")
    private String year;

    @NotNull
    @Column(name = "Month")
    private String month;

    @NotNull
    @Column(name = "RunningNo")
    private Long runningNo;

}
