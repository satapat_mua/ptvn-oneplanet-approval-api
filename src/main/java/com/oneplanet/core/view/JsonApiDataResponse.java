package com.oneplanet.core.view;

import java.util.List;

import lombok.Data;

/**
 * Created by neng on 3/8/2017 AD.
 */
@Data
public class JsonApiDataResponse<T> {
    private List<ObjectWithDataAndMetaResponse<T>> data;
}