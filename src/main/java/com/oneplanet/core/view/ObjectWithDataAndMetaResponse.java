package com.oneplanet.core.view;

import java.util.List;

import lombok.Data;

/**
 * Created by neng on 3/8/2017 AD.
 */
@Data
public class ObjectWithDataAndMetaResponse<T> {
    private String type;

    private String id;

    private List<T> attributes;
}
