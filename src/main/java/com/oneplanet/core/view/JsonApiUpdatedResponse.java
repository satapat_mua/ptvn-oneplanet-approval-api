package com.oneplanet.core.view;

import java.util.List;

import lombok.Data;

/**
 * Created by neng on 3/8/2017 AD.
 */
@Deprecated
@Data
public class JsonApiUpdatedResponse<T> {
    private List<ObjectWithUpdatedResponse<T>> data;

}