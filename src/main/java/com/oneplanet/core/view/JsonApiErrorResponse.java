package com.oneplanet.core.view;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by neng on 3/8/2017 AD.
 */
@Data
public class JsonApiErrorResponse {
    @NonNull
    private String title;

    @NonNull
    private Integer status;

    @NonNull
    private String detail;

    private List<JsonApiErrorField> errors = newArrayList();

    public void addField(String field, String message) {
        this.errors.add(new JsonApiErrorField(field, message));
    }
}