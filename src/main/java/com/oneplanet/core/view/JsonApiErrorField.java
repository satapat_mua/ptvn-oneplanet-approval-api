package com.oneplanet.core.view;

import lombok.Data;
import lombok.NonNull;

/**
 * Created by neng on 3/8/2017 AD.
 */
@Deprecated
@Data
public class JsonApiErrorField {
    @NonNull
    private String field;

    @NonNull
    private String message;
}
