package com.oneplanet.core.view;

import java.util.List;

import lombok.Data;

import com.oneplanet.core.meta.Meta;

/**
 * Created by neng on 3/8/2017 AD.
 */
@Data
public class JsonApiDataAndMetaResponse<T> {
    private List<ObjectWithDataAndMetaResponse<T>> data;

    private List<Meta> meta;
}