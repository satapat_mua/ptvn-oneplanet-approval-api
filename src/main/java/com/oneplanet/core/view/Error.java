package com.oneplanet.core.view;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by sattra.sow on 9/8/2017 AD.
 */
@Data
@AllArgsConstructor
public class Error {

    private String code;

    private String message;

}
