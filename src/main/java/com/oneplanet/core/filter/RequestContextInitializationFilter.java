package com.oneplanet.core.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.oneplanet.core.utils.RequestContext;

/**
 * The RequestContextInitializationFilter is executed for every web request. The filter initializes the RequestContext
 * for the current thread, preventing leaking of RequestContext attributes from the previous thread's execution.
 *
 * @author Matt Warman
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestContextInitializationFilter extends GenericFilterBean {

    /**
     * The Logger for this class.
     */
    private static final Logger logger = LoggerFactory.getLogger(RequestContextInitializationFilter.class);

    @Autowired
    ServletContext servletContext;

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain chain)
            throws IOException, ServletException {
        logger.info("> doFilter initialFilter");

        RequestContext.init();

        HttpServletRequest servletRequest = (HttpServletRequest) req;
        HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper(servletRequest);

        String token = requestWrapper.getHeader("token");
        if (token != null) {
            int tokenDotIndex = token.lastIndexOf('.');
            String withoutSignature = token.substring(0, tokenDotIndex + 1);
            Jwt<Header,Claims> untrusted = Jwts.parser().parseClaimsJwt(withoutSignature);
            Claims claims = untrusted.getBody();
            requestWrapper.addHeader("userId", String.valueOf(claims.get("userId")));
            requestWrapper.addHeader("userName", String.valueOf(claims.get("userName")));
            requestWrapper.addHeader("firstName", String.valueOf(claims.get("firstName")));
            requestWrapper.addHeader("lastName", String.valueOf(claims.get("lastName")));
            requestWrapper.addHeader("fullName", String.valueOf(claims.get("fullName")));
            servletContext.setAttribute("userId", String.valueOf(claims.get("userId")));
            servletContext.setAttribute("userName", String.valueOf(claims.get("userName")));
            servletContext.setAttribute("firstName", String.valueOf(claims.get("firstName")));
            servletContext.setAttribute("lastName", String.valueOf(claims.get("lastName")));
            servletContext.setAttribute("fullName", String.valueOf(claims.get("fullName")));
        } else {
            servletContext.removeAttribute("userId");
            servletContext.removeAttribute("userName");
            servletContext.removeAttribute("firstName");
            servletContext.removeAttribute("lastName");
            servletContext.removeAttribute("fullName");
        }

        String supplierToken = requestWrapper.getHeader("supplierToken");

        if (supplierToken != null) {
            int tokenDotIndex = supplierToken.lastIndexOf('.');
            String withoutSignature = supplierToken.substring(0, tokenDotIndex + 1);
            Jwt<Header,Claims> untrusted = Jwts.parser().parseClaimsJwt(withoutSignature);
            Claims claims = untrusted.getBody();
            requestWrapper.addHeader("userId", String.valueOf(claims.get("user_id")));
            requestWrapper.addHeader("userName", String.valueOf(claims.get("user_name")));
            servletContext.setAttribute("userId", String.valueOf(claims.get("user_id")));
            servletContext.setAttribute("userName", String.valueOf(claims.get("user_name")));
        } else {
            servletContext.removeAttribute("userId");
            servletContext.removeAttribute("userName");
        }
        // Enable this section when implement to verify token
        /*else {
            ((HttpServletResponse) response)
                    .setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }*/

        // Goes to default servlet
        chain.doFilter(requestWrapper, resp);

        logger.info("< doFilter initialFilter");
    }

}
