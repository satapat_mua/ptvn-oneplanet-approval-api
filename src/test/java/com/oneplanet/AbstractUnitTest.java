package com.oneplanet;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import com.oneplanet.core.utils.RequestContext;

/**
 * The AbstractTest class is the parent of all JUnit test classes. This class configures the test ApplicationContext and
 * test runner environment.
 *
 * @author Matt Warman
 */
public abstract class AbstractUnitTest {

    /**
     * The username value used in the RequestContext for Unit Tests.
     */
    public static final String USERNAME = "unittest";

    @Before
    public void before() {
        RequestContext.setUsername(AbstractUnitTest.USERNAME);
        doBeforeEachTest();
    }

    /**
     * Perform initialization tasks before the execution of each test method.
     */
    public abstract void doBeforeEachTest();

    @After
    public void after() {
        doAfterEachTest();
    }

    /**
     * Perform clean up tasks after the execution of each test method.
     */
    public abstract void doAfterEachTest();

    /**
     * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
     *
     * @param obj The Object to map.
     * @return A String of JSON.
     * @throws JsonProcessingException Thrown if an error occurs while mapping.
     */
    protected String mapToJson(final Object obj) throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        return mapper.writeValueAsString(obj);
    }

    /**
     * Maps a String of JSON into an instance of a Class of type T. Uses a Jackson ObjectMapper.
     *
     * @param json  A String of JSON.
     * @param clazz A Class of type T. The mapper will attempt to convert the JSON into an Object of this Class type.
     * @return An Object of type T.
     * @throws JsonParseException   Thrown if an error occurs while mapping.
     * @throws JsonMappingException Thrown if an error occurs while mapping.
     * @throws IOException          Thrown if an error occurs while mapping.
     */
    protected <T> T mapFromJson(final String json, final Class<T> clazz)
            throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new JodaModule());
        return mapper.readValue(json, clazz);
    }

}
