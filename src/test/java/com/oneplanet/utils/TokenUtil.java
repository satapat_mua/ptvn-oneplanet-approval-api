package com.oneplanet.utils;

import java.security.Key;
import java.util.Date;
import java.util.UUID;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenUtil {

    public String generateToken() throws Exception {
        Claims claims = Jwts.claims();
        claims.setId(String.valueOf(UUID.randomUUID()));
        claims.setIssuedAt(new Date());
        claims.put("userId", 132L);
        claims.put("userName", "testUser");

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, getSigningKey())
                .compact();
    }

    private Key getSigningKey() {
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("OnePlanetSupplierAPI");
        return new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS512.getJcaName());
    }
}
