#!/bin/bash
echo "Starting Install robotframework library"
pip install robotframework
pip install robotframework-selenium2library
echo "Finish Installed"

echo "Starting .. Git Clone PTVN - Automate"
git clone -b develop https://supawan_nga:supawan.nga@bitbucket.org/ascendcorp/ptvn-automation.git
echo "Git Clone complete !!!"
pwd
echo "Starting .. Execute Smoke Test"
pybot --output original.xml -d smoketest_result -v ENV:staging -i smoketest ptvn-automation
cd smoketest_result
result=$(if grep -q 'fail="0"' original.xml; then echo "Passed"; else echo "Failed"; fi)
echo "SmokeTest Result : " $result
if $result != *"Passed"* ; then echo "Starting Rerun ..." ; fi
if $result != *"Passed"* ; then pybot --rerunfailed original.xml --output rerun.xml -d smoketest_result -i smoketest ptvn-automation ; fi
if [[ $result != *"Passed"* ]]; then rebot --merge original.xml rerun.xml ; fi
#test