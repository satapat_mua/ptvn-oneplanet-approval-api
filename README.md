# OnePlanet Online Purchasing Services
This is an online purchasing web services API.

## Features
- [x] Approval API
  
## Getting Started
```sh
# clone it
git clone git@bitbucket.org:ascendcorp/ptvn-oneplanet-approval-api.git

# Install dependencies
gradle build

# Run check-style, pmd and unit tests
gradle check
```

## License
Copyright (c) 2018 Pantavanij.com All Right Reserved